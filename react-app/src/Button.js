import React from 'react';

const Button = ({ number }) => {
  const numberText = () => {
    if (number === 1) {
      return <div>i am number one</div>;
    }

    return number;
  };
  return <button>{numberText()}</button>;
};
export default Button;
