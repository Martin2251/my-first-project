import React from 'react';
import Item from './Item';

const items = [
  { id: 1, text: 'go to shops'},
  { id: 2, text: 'go for a run'},
  { id: 3, text: 'make the bed'},
]

const List = () => {
  return (
    <div>
      <div>list title</div>
      {items.map((item) => {
        return <Item item={item} />;
      })}
    </div>
  );
};

export default List;
