import React, { useState } from 'react';
import TodoForm from './TodoForm';
import Todo from './Todo';

function TodoList() {
  const [todos, setTodos] = useState([]);

  const addTodo = (todo) => {
    if (!todo.text || /^\s*$/.test(todo.text)) {
      return;
    }
    const newTodos = [todo, ...todos];

    setTodos(newTodos);
  };

  todo : [
    {id : 1, task : "make the bed"},
    {id : 2, task : "do some coding"},
    {id : 3, task : "go for a run"},
    {id : 4, task : "do the washing"},

  ]

  // const updateTodos = (todoId) => {
  //   // if (!newValue.text || /^\s*$/.test(newValue.text)) {
  //     return;
  //   }
  //   setTodos((prev) =>
  //     prev.map((item) => (item.id === todoId ? newValue : item))
  //   );
  // };

  const removeTodo = (id) => {
    const removeArr = [...todos].filter((todo) => todo.id !== id);
    setTodos(removeArr);
  };

  const completeTodo = (id) => {
    let updatedTodos = todos.map((todo) => {
      if (todo.id == id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });
    setTodos(updatedTodos);
  };



  return (
    <div>
      <h1>What is your Plan for Today?</h1>
      <TodoForm onSubmit={addTodo} />
      <Todo
        todos={todos}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
        // updateTodos={updateTodo}
      />
    </div>
  );
}

 class App extends Component {
    constructor (props) {
      super(props);
      this.state= {
        userInput: "",
        toDoList : []
      }
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleSubmit =this.handleChange.bind(this);
    }

    handleSubmit() {
      const itemsArray =this.state.userInput.split(',');
      this.setState({
        toDoList: itemsArray
      });
    }
    handleChange(e) {
      this.setState({
        userInput: e.target.value

      });
    }

      render() {
        const items = this.state.toDoList.map(element => {
          return <li>{element}</li>
        });
        return (
          <div>
            <textarea
            onChange={this.handleChange}
            value={this.state.userInput}
            style={textAreaStyles}
            placeholder="Seperate Items With Commas"  />
            <br />
            <button onClick={this.handleSubmit}>Create List</button>
            <h1>My "To Do" List</h1>
            <ul>
              {items}
              </ul>
              </div>
        );
        }
      };

export default TodoList;
