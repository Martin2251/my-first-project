import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';

class AddTodo extends Component {
    state = {
        defaultValue: "",
        value: this.props.addTodoValue
    }
    // default valuer nothing and a string

    handleChange = (e) => {
        //Updating local component state
        this.setState({
            value: e.target.value
        });
    }

    clearInput = () => {
        //Clear existing value in input
        document.getElementById("todoValue").value = "";

        //Updating local component state
        this.setState({value:""});
    }

    addTodo = () => {
        //Call method reference in Todos component using props
        this.props.fooAddTodo(this.state.value);
        this.clearInput();
    }

    render() {
        return (
            <div className="input-group mb-3">
                <input type="text" className="form-control" id="todoValue" placeholder="ToDo" onChange={this.handleChange} />
                <div className="input-group-append">

                     <Button onClick={this.addTodo} variant="success" type="button" id="button-addon2">Add a New ToDo </Button>

                </div>
            </div>
        );
    }
}

export default AddTodo;
